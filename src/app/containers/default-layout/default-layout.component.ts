import { Component, OnDestroy, Inject, OnInit } from '@angular/core';
import { DOCUMENT } from '@angular/common';
import { navItems, navDataObj } from './../../_nav';
import { HttpService } from '../../services';

import constants from '../../app.constants';
@Component({
  selector: 'app-dashboard',
  templateUrl: './default-layout.component.html'
})
export class DefaultLayoutComponent implements OnDestroy, OnInit {
  public navItems = navItems;
  public sidebarMinimized = true;
  private changes: MutationObserver;
  public element: HTMLElement;
  public authData: any;

  constructor(private service: HttpService, @Inject(DOCUMENT) _document?: any) {
    this.changes = new MutationObserver(mutations => {
      this.sidebarMinimized = _document.body.classList.contains(
        'sidebar-minimized'
      );
    });
    this.element = _document.body;
    this.changes.observe(<Element>this.element, {
      attributes: true,
      attributeFilter: ['class']
    });
  }

  ngOnInit() {

    this.authData = JSON.parse(localStorage.getItem('coreui-auth'));
    const authority = this.authData.authorities[0].authority;

    this.service
      .post(
        `${constants.BASE_URL}/api/common/menu/list/${authority}`,
        this.authData,
        true
      )
      .subscribe(
        response => {
          this.navItems = [];
          response.forEach(element => {
            this.navItems.push(
              new navDataObj(
                element.name,
                element.icon,
                element.url,
                element.childmenu
              )
            );
          });
        }
      );
  }
  ngOnDestroy(): void {
    this.changes.disconnect();
  }

  logout() {
    console.log('clicked logout');

    this.service.logout();
  }
}
