import { Injectable } from '@angular/core';
import { HttpInterceptor, HttpEvent, HttpHandler, HttpRequest, HttpErrorResponse, HttpResponse } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { HttpService } from '../services';
import { map, catchError } from 'rxjs/operators';
import constants from '../app.constants';
import { AlertService } from '../components/alert/alert.service';

@Injectable()
export class HttpInterceptors implements HttpInterceptor {
    constructor(private services: HttpService, private alertService: AlertService) {}

    intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        request = request.clone({
            setHeaders: {
                Authorization: `Bearer ${this.services.getToken()}`,
                'Content-Type': 'application/json'
            }
        });

        return next.handle(request)
            .pipe(map((event: HttpEvent<any>) => {
                if (event instanceof HttpResponse) {
                    console.log('event ==> ', event);
                }

                return event;
            }), catchError((error: HttpErrorResponse) => {
                let data = {};

                data = {
                    reason: error && error.error.reason ? error.error.reason : '',
                    status: error.status
                };

                if (data['reason'] || data['reason'] === '') {
                    const _data = [{
                        message: `${constants.ERROR_MESSAGES[data['status']]}`,
                        type: 'danger',
                        timeout: 5000
                    }];

                    this.alertService.showAlert(_data);
                } else {
                    alert(`${data['reason']}`);
                    const _data = [{
                        message: `${data['reason']}`,
                        type: 'danger',
                        timeout: 5000
                    }];

                    this.alertService.showAlert(_data);
                }
                return throwError(error);
            }));
    }
}
