import { Component, OnInit } from '@angular/core';
import {
  Router,
  NavigationEnd,
  ActivationEnd,
  ActivationStart
} from '@angular/router';
import { HttpService } from '../app/services';

@Component({
  // tslint:disable-next-line
  selector: 'body',
  template: '<router-outlet></router-outlet>'
})
export class AppComponent implements OnInit {
  constructor(private router: Router, private services: HttpService) {}

  ngOnInit() {
    this.router.events.subscribe(evt => {
      if (!(evt instanceof NavigationEnd)) {
        return;
      } else {
        const urlSplits = evt.url.split('/');
        if (urlSplits.length > 2) {
          if (urlSplits[1] !== this.services.getRole()) {
            this.services.logout();
          }
        }
      }

      window.scrollTo(0, 0);
    });
  }
}
