import { Component, Inject } from "@angular/core";
import {
  FormControl,
  Validators,
  FormGroup,
  AbstractControl,
  ReactiveFormsModule
} from "@angular/forms";
import { HttpService } from "../../services";
import { Router } from "@angular/router";

import constants from "../../app.constants";

import { UserIdleService } from "angular-user-idle";

@Component({
  selector: "app-dashboard",
  templateUrl: "login.component.html"
})
export class LoginComponent {
  formSubmitted: Boolean = false;
  formData: any;

  constructor(
    private service: HttpService,
    private router: Router,
    private sessionTimeout: UserIdleService
  ) {
    console.log("cons", constants);
  }
  loginForm = new FormGroup({
    username: new FormControl("", [Validators.required]),
    password: new FormControl("", [Validators.required]),
    rememberme: new FormControl(false)
  });
  onLogin() {
    if (!this.loginForm.valid) {
      this.formSubmitted = true;
      return this.loginForm;
    }

    this.formData = this.loginForm.value;
    delete this.formData.rememberme;

    this.service
      .post(`${constants.BASE_URL}/api/auth/signin`, this.formData, false)
      .subscribe(response => {
        // If remember me is false, Session Logout is disabled
        if (!this.loginForm.value.rememberme) {
          this.setWatchers();
        }

        this.service.setAuthData(response);
        const role = this.service.getRole();

        this.router.navigateByUrl(`${role}/dashboard`);
      });
  }

  setWatchers() {
    this.sessionTimeout.startWatching();

    this.sessionTimeout.onTimerStart().subscribe(count => {
      console.log('countdown ', count);
    });

    this.sessionTimeout.onTimeout().subscribe(() => {
      this.service.logout();
    });
  }
}
