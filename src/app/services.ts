import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Http, Headers, RequestOptions, Response } from '@angular/http';
import { Observable } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { Router } from '@angular/router';

@Injectable()
export class HttpService {
  authData: any;

  constructor(private http: HttpClient, private router: Router) {}

  post(url, data, token): Observable<any> {
    let dataVal = {};
    if (token) {
      dataVal = {
        name: 'name 1',
        parent_id: '1',
        name_native: 'name_native 1',
        type: 'DIVISION'
      };
    } else {
      dataVal = data;
    }

    return this.http.post(url, dataVal);
  }

  getRole(): String {
    const auth = localStorage.getItem('coreui-auth');
    if (!auth) {
      return auth;
    }

    const authority = JSON.parse(auth).authorities[0].authority;

    if (authority) {
      return authority.toLowerCase().replace('role_', '');
    }
  }

  isAuthenticated(): Boolean {
    let isLoggedIn = false;
    const auth = localStorage.getItem('coreui-auth');

    if (auth) {
      const coreAuth = JSON.parse(auth);
      if (coreAuth.accessToken) {
        isLoggedIn = true;
      }
    }

    return isLoggedIn;
  }

  setAuthData(data) {
    this.authData = data;
    localStorage.setItem('coreui-auth', JSON.stringify(data));
  }

  getAuthData() {
    return JSON.parse(localStorage.getItem('coreui-auth'));
  }

  getToken() {
    let coreAuth = localStorage.getItem('coreui-auth');

    if (coreAuth) {
      coreAuth = JSON.parse(coreAuth);
      return coreAuth['accessToken'];
    }

    return null;
  }

  logout() {
    // clears any keys set when loggedIn.
    localStorage.removeItem('coreui-auth');
    this.router.navigate(['login']);
  }
}
