import { Subject } from 'rxjs';

export class AlertService {
    messages = new Subject<Object>();

    constructor() {

    }

    showAlert(data) {
        this.messages.next(data);
    }
}
