import { Component, OnInit, SecurityContext } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';

import { AlertService } from './alert.service';

@Component({
  selector: 'app-alerts',
  templateUrl: './alert.component.html',
  styleUrls: ['./alert.component.scss']
})
export class AlertComponent implements OnInit {
  alerts: any = [];
  dismissible: Boolean = true;

  constructor(
    public sanitizer: DomSanitizer,
    private alertService: AlertService
  ) {}

  ngOnInit() {
    this.alertService.messages.subscribe((alerts: any) => {
      this.alerts = alerts.map(alert => ({
        msg: alert.message,
        type: alert.type,
        timeout: alert.timeout
      }));
    });
  }
}
