import { NgModule, ModuleWithProviders } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';
import { AlertModule, AlertConfig } from 'ngx-bootstrap/alert';

import { AlertComponent } from './alert.component';
import { AlertService } from './alert.service';
import { Alert } from 'selenium-webdriver';

@NgModule({
  imports: [CommonModule, FormsModule, AlertModule],
  declarations: [AlertComponent],
  providers: [AlertService, AlertConfig],
  exports: [AlertComponent]
})
export class AlertsModule {}
